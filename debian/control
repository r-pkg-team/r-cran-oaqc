Source: r-cran-oaqc
Section: gnu-r
Priority: optional
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-oaqc
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-oaqc.git
Homepage: https://cran.r-project.org/package=oaqc
Standards-Version: 4.7.0
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev
Testsuite: autopkgtest-pkg-r

Package: r-cran-oaqc
Architecture: any
Depends: ${R:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GNU R computation of the orbit-aware quad census
 Implements the efficient algorithm by Ortmann and Brandes (2017)
 <doi:10.1007/s41109-017-0027-2> to compute the orbit-aware frequency
 distribution of induced and non-induced quads, i.e. subgraphs of size
 four. Given an edge matrix, data frame, or a graph object
 (e.g., 'igraph'), the orbit-aware counts are computed respective each
 of the edges and nodes.
